package com.speaktome.android.sensorsimulation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.speaktome.android.sensorsimulation.bluetooth.Bluetooth;
import com.speaktome.android.sensorsimulation.bluetooth.ConnectThread;
import com.speaktome.android.sensorsimulation.bluetooth.ManageThread;

public class MainActivity extends Activity implements SensorEventListener {
	
	public static final int BLUETOOTH_ENABLE_REQUEST = 12345;
	
	TextView x;
	TextView y;
	TextView z;
	SensorManager sensorManager;
	Sensor accelerometer;

	String[] pairedDevicesAddresses;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		x = (TextView) findViewById(R.id.textView_x_val);
		y = (TextView) findViewById(R.id.textView_y_val);
		z = (TextView) findViewById(R.id.textView_z_val);
		Button start = (Button) findViewById(R.id.btn_start);
		ToggleButton send = (ToggleButton) findViewById(R.id.btn_send);
		Button stop = (Button) findViewById(R.id.btn_stop);
		
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		pairedDevicesAddresses = new String[0];
				
		start.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				sensorManager.registerListener(MainActivity.this, accelerometer, SensorManager.SENSOR_DELAY_UI);
			}
		});

		stop.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				sensorManager.unregisterListener(MainActivity.this, accelerometer);
			}
		});
		
		send.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) {
				if(isChecked){
					if(!Bluetooth.connected){  // TODO: should receive all Bluetooth activity 
						BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
						if(!bluetoothAdapter.isEnabled()) {
							Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
							MainActivity.this.startActivityForResult(enableIntent, BLUETOOTH_ENABLE_REQUEST);
						} else {
							onActivityResult(BLUETOOTH_ENABLE_REQUEST, RESULT_OK, MainActivity.this.getIntent());
						}
					}else {
						startCommunication();
					}
				} else{
					Bluetooth.communicating = false;
				}
			}
			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			while(bluetoothAdapter.getState() == BluetoothAdapter.STATE_TURNING_ON) {
			}
			if(bluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF){
				Toast.makeText(getApplicationContext(), "Bluetooth failed..", Toast.LENGTH_SHORT).show();
				return;
			}
			Set<BluetoothDevice> pairedDevicesSet = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
			if(!pairedDevicesSet.isEmpty()){
				ArrayList<String> pairedDevicesArrayList = new ArrayList<String>();
				Iterator<BluetoothDevice> iterator = pairedDevicesSet.iterator();
				while(iterator.hasNext()){
					pairedDevicesArrayList.add(iterator.next().getAddress());
				}
				pairedDevicesAddresses = pairedDevicesArrayList.toArray(pairedDevicesAddresses);
			}
			if(pairedDevicesAddresses.length>0){
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
				alertDialogBuilder.setItems(pairedDevicesAddresses, new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String MAC = pairedDevicesAddresses[which];
						BluetoothDevice bluetoothDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(MAC);
						Bluetooth.connectThread = new ConnectThread(bluetoothDevice);
						Bluetooth.connectThread.start();
						while(Bluetooth.connectThread.isAlive()){}
						if(Bluetooth.connected){
							Toast.makeText(getApplicationContext(), "Connection successful..", Toast.LENGTH_SHORT).show();
							startCommunication();
						} else{
							Toast.makeText(getApplicationContext(), "Connection failed..", Toast.LENGTH_SHORT).show();
						}
//						Bluetooth.acceptThread = new AcceptThread();
//						Bluetooth.acceptThread.start();
					}
				});
				alertDialogBuilder.show();
			}
			
			Toast.makeText(getApplicationContext(), "connecting thread", Toast.LENGTH_SHORT).show();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		Float float_x = event.values[0];
		Float float_y = event.values[1];
		Float float_z = event.values[2];
		
//		x.setText(String.valueOf(float_x));
//		y.setText(String.valueOf(float_y));
//		z.setText(String.valueOf(float_z));
		
		x.setText(String.valueOf(float_x.byteValue()));
		y.setText(String.valueOf(float_y.byteValue()));
		z.setText(String.valueOf(float_z.byteValue()));
		
		if(Bluetooth.communicating){
			Bluetooth.manageThread.write(new byte[]{float_x.byteValue(), float_y.byteValue(), float_z.byteValue()});
		}
	}
	
	void startCommunication(){
		if(Bluetooth.connected){
			if(!Bluetooth.communicating){
				Bluetooth.manageThread = new ManageThread(Bluetooth.bluetoothSocket);
			}
			if(Bluetooth.communicating){
				if(!Bluetooth.manageThread.isAlive()){
					Bluetooth.manageThread.start(); //for reading msgs
				}
				Toast.makeText(getApplicationContext(), "Communication started..", Toast.LENGTH_SHORT).show();
			} else{
				Toast.makeText(getApplicationContext(), "Communication failed..", Toast.LENGTH_SHORT).show();
			}
		} else{
			Toast.makeText(getApplicationContext(), "Device not connected..", Toast.LENGTH_SHORT).show();
		}
	}

}
