package com.speaktome.android.sensorsimulation.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothSocket;
import android.os.HandlerThread;
import android.util.Log;

public class ManageThread extends HandlerThread{
	
	public final BluetoothSocket bluetoothSocket;
	public final InputStream inStream;
	public final OutputStream outStream;
	
	public ManageThread(BluetoothSocket socket) {
		super("ManageThread");
		bluetoothSocket = socket;
		InputStream tempi = null;
		OutputStream tempo = null;
		try {
			tempi = bluetoothSocket.getInputStream();
			tempo = bluetoothSocket.getOutputStream();
			Bluetooth.communicating = true;
		} catch (IOException e) {
			Log.e("BLUE ERROR - MANAGE", e.getMessage());
//			stop
		}
		inStream = tempi;
		outStream = tempo;
	}

	/*define the length of the message to be read
	keep listening to input stream and increment counter on successful read operation
	break if expected no of bytes have been read and pass notifications accordingly
	restart process*/
	@Override
	public void run() {
		int arrayLenght = 8;	//size of single message is assumed to be 8bytes
		byte[] buffer;
		int counter;
		while(true) {
			buffer = new byte[arrayLenght];
			counter = 0;
			while(true) {
				try {
					if((buffer[counter] = (byte) inStream.read()) != -1){
						counter++;
					}
					if(counter == arrayLenght){
						break;
					}
				} catch (IOException e) {
				}
			}
//			String msg = new String(buffer);
//			String prefix = msg.substring(0, 2);	//differentiate between StickEras and StickFinders messages
//			String id = msg.substring(2);
//			if(prefix.equals("SF")) {	//if message is from StickFinders
//				showNotificationStickFinder(getBaseContext(), id);
//			}else if(prefix.equals("SE")){	//if message is from StickEars
//				selectNotif(id);
//			}
		}
	}

	public boolean write(byte[] msg) {
		try {
			outStream.write(msg);
			return true;
		} catch (IOException e) {
			Log.e("BLUETOOTH ERROR MANAGE", e.getMessage());
			return false;
		}
	}
	
}
