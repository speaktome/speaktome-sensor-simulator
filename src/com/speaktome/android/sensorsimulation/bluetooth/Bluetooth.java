package com.speaktome.android.sensorsimulation.bluetooth;

import android.bluetooth.BluetoothSocket;


public class Bluetooth {
	public static AcceptThread acceptThread;
	
	public static ConnectThread connectThread;
	public static boolean connected = false;
	public static BluetoothSocket bluetoothSocket;
	
	public static ManageThread manageThread;
	public static boolean communicating = false;
}