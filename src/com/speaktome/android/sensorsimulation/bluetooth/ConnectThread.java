package com.speaktome.android.sensorsimulation.bluetooth;

import java.io.IOException;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class ConnectThread extends Thread {
	
	public final BluetoothDevice bluetoothDevice;
	public final BluetoothSocket bluetoothSocket;

	public ConnectThread(BluetoothDevice device) {
		this.bluetoothDevice = device;
		BluetoothSocket temp = null;
		try {
			temp = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
		} catch (Exception e) {
			Log.e("BLUETOOTH ERROR CONNECT", e.getMessage());
			Bluetooth.connected = false; 
		}
		this.bluetoothSocket = temp;
	}

	@Override
	public void run() {
		BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
		try {
			bluetoothSocket.connect();
		} catch (IOException e) {
			try {
				Log.e("BLUETOOTH ERROR CONNECT", e.getMessage());
				Bluetooth.connected = false;			
				bluetoothSocket.close();
			} catch (IOException e1) {
				Log.e("BLUETOOTH ERROR CONNECT", e1.getMessage());
			}
			return;
		}
		Bluetooth.connected = true;
		Bluetooth.bluetoothSocket = bluetoothSocket;
	}
	
	public boolean close() {
		try {
			Bluetooth.connected = false;
			bluetoothSocket.close();
			return true;
		} catch (IOException e) {
			Log.e("BLUETOOTH ERROR CONNECT", e.getMessage());
			return false;
		}
	}
}
