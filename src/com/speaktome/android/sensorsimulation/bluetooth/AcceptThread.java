package com.speaktome.android.sensorsimulation.bluetooth;

import java.io.IOException;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class AcceptThread extends Thread {
    public final BluetoothServerSocket mmServerSocket;
 
    public AcceptThread() {
        BluetoothServerSocket temp = null;
        try {
            temp = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("SPEAK", UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
        } catch (IOException e) {
        	Log.e("BLUE ERROR", "listenUsingRfcommWithServiceRecord"); ////////////////
        	Bluetooth.connected = false;
        }
        mmServerSocket = temp;
    }
 
    public void run() {
        BluetoothSocket socket = null;
        // Keep listening until exception occurs or a socket is returned
        while (true) {
            try {
                socket = mmServerSocket.accept();
            } catch (IOException e) {
            	Log.e("BLUE ERROR", "listenUsingRfcommWithServiceRecord"); ////////////////
            	Bluetooth.connected = false;
                break;
            }
            if (socket != null) {
            	Log.e("BLUE ERROR", "yeeessss......."); ////////////////
            	Bluetooth.connected = true;
                break;
            }
        }
    }
 
    public void cancel() {
        try {
            mmServerSocket.close();
        } catch (IOException e) { }
    }
}